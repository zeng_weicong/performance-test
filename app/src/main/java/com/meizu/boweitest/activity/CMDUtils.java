package com.meizu.boweitest.activity;

import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * 执行命令
 */
public class CMDUtils {

    private static final String TAG = "CMDUtils";
    private static Process process = null;
    public static class CMD_Result {
        public int resultCode;
        public String error;
        public String success;

        public CMD_Result(int resultCode, String error, String success) {
            this.resultCode = resultCode;
            this.error = error;
            this.success = success;
        }

    }
    /**
     * 执行命令
     *
     * @param command         命令
     * @param isShowCommand   是否显示执行的命令
     * @param isNeedResultMsg 是否反馈执行的结果
     * @retrun CMD_Result
     */
    public static CMD_Result runCMD(String command, boolean isShowCommand,
                                    boolean isNeedResultMsg) {
        if (isShowCommand)
            Log.i(TAG, "runCMD:" + command);
        CMD_Result cmdRsult = null;
        try {
            process= Runtime.getRuntime().exec(command);
            if (isNeedResultMsg) {
                final StringBuilder successMsg = new StringBuilder();
                final StringBuilder errorMsg = new StringBuilder();
                //解决调用cmd命令阻塞问题。启动两个线程，一个线程负责读标准输出流，另一个负责读标准错误流
                new Thread() {
                    public void run() {
                        BufferedReader successResult = new BufferedReader(
                                new InputStreamReader(process.getInputStream()));
                        try {
                            String line1 = null;
                            while ((line1 = successResult.readLine()) != null) {
                                successMsg.append(line1);
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        finally{
                            try {
                                successResult.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }.start();
                new Thread() {
                    public void  run() {
                        BufferedReader errorResult = new BufferedReader(
                                new InputStreamReader(process.getErrorStream()));
                        try {
                            String line2 = null ;
                            while ((line2 = errorResult.readLine()) !=  null ) {
                                errorMsg.append(line2);
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        finally{
                            try {
                                errorResult.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }.start();
                process.waitFor();
                process.destroy();
                cmdRsult = new CMD_Result(process.waitFor(), errorMsg.toString(),
                        successMsg.toString());
            }
        } catch (Exception e) {
            Log.e(TAG, "run CMD:" + command + " failed");
            e.printStackTrace();
            try{
                process.getErrorStream().close();
                process.getInputStream().close();
                process.getOutputStream().close();
            }
            catch(Exception ee)
            {
            }
        }
        return cmdRsult;
    }

}