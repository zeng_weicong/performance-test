package zengweicong.com.performancetest.utils;

import android.content.Context;
import android.os.BatteryManager;
import android.os.Build;
import android.util.Log;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.util.Locale;

/**
 * Current info
 * 
 */
public class CurrentInfo {
	public static boolean needProcess = false;
	private static boolean implementInterface = true;
	private static final String LOG_TAG = "Performance-CurrentInfo";
	private static final String BUILD_MODEL = Build.MODEL.toLowerCase(Locale.ENGLISH);
	private static final String I_MBAT = "I_MBAT: ";
	private static final String CURRENT_NOW = "/sys/class/power_supply/battery/current_now";
	private static final String BATT_CURRENT = "/sys/class/power_supply/battery/batt_current";
	private static final String SMEM_TEXT = "/sys/class/power_supply/battery/smem_text";
	private static final String BATT_CURRENT_ADC = "/sys/class/power_supply/battery/batt_current_adc";
	private static final String CURRENT_AVG = "/sys/class/power_supply/battery/current_avg";

	/**
	 * read system file to get current value
	 * 
	 * @return current value
	 */
	public float getCurrentValue(Context context) {
		float current = 0;
		// Android 5.0及以上接口正常可以直接通过系统调用获取当前电流值
		if (!implementInterface && Build.VERSION.SDK_INT >= 26) {
			return -1;
		}
		if (implementInterface && android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && context != null)
		{
			BatteryManager mBatteryManager = (BatteryManager) context.getSystemService(Context.BATTERY_SERVICE);
			if (mBatteryManager == null) {
				return -1;
			}
			current=mBatteryManager.getLongProperty(BatteryManager.BATTERY_PROPERTY_CURRENT_NOW);

			// 保证用电时为正，充电时为负
			current = -current;

			// 微安为单位特殊处理
			if (current > 100000 || current < -100000) {
				needProcess = true;
			}

			if (needProcess) {
				current = current / 1000;
			}
			Log.d("battery","battery"+current);
			return current;
		}
		else {
			// 对于Android5.0以下系统，只能读取 /sys/class/power_supply/ 下一个包含battery的文件夹中的包含 current_now文件读取当前电流值
			File f =null;
			f = new File(CURRENT_NOW);
			if (f.exists())
				return getCurrentValue(f, true);
		}
		return -1;
	}

	/**
	 * get current value from smem_text
	 * 
	 * @return current value
	 */
	public Long getSMemValue() {
		boolean success = false;
		String text = null;
		Long value = null;
		try {
			FileReader fr = new FileReader(SMEM_TEXT);
			BufferedReader br = new BufferedReader(fr);
			String line = br.readLine();
			while (line != null) {
				if (line.contains(I_MBAT)) {
					text = line.substring(line.indexOf(I_MBAT) + 8);
					success = true;
					break;
				}
				line = br.readLine();
			}
			fr.close();
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (success) {
			try {
				value = Long.parseLong(text);
			} catch (NumberFormatException nfe) {
				nfe.printStackTrace();
				value = null;
			}
		}
		return value;
	}

	/**
	 * read system file to get current value
	 * 
	 * @param file
	 * @param convertToMillis 
	 * @return current value
	 */
	public Long getCurrentValue(File file, boolean convertToMillis) {
		Log.d(LOG_TAG, "*** getCurrentValue ***");
		Log.d(LOG_TAG, "*** " + convertToMillis + " ***");
		String line = null;
		Long value = null;
		FileInputStream fs = null;
		DataInputStream ds = null;
		try {
			fs = new FileInputStream(file);
			ds = new DataInputStream(fs);
			line = ds.readLine();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				fs.close();
				ds.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		if (line != null) {
			try {
				value = Long.parseLong(line);
			} catch (NumberFormatException nfe) {
				value = null;
			}
			if (convertToMillis)
				value = value / 1000;
		}
		return value;
	}
}
